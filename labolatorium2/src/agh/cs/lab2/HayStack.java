package agh.cs.lab2;

public class HayStack {
	private Position placement;
	
	public HayStack(Position position){
		this.placement = position;
	}
	
	public Position getPosition(){
		return this.placement;
	}
	
	public String toString(){
		return "s";
	}
}
