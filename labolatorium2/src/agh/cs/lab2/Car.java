package agh.cs.lab2;

public class Car {
	private Position pos;
	private MapDirection dir;
	private IWorldMap mapa;
	
	private final static Position SWboundary = new Position(0,0);
	private final static Position NEboundary = new Position(4,4);
	
	
	public Car(){
		this.pos=new Position(2, 2);
		this.dir=MapDirection.NORTH;
	}
	
	public Car(IWorldMap map){
		this.mapa = map;
		this.pos=new Position(2, 2);
		this.dir=MapDirection.NORTH;
	}
	
	public Car(IWorldMap map, int x, int y){
		this.mapa = map;
		this.pos=new Position(x, y);
		this.dir=MapDirection.NORTH;
	}
	
	public String toString(){
		 switch(dir) {
         case NORTH: return "N";
         case SOUTH: return "S";
         case EAST: return "E";
         case WEST: return "W";
         default : return "";
     }
	}
	
	public void move(MoveDirection m){
		
		switch(m){
		case RIGHT : dir = (MapDirection) dir.next();
		break;
		case LEFT : dir = (MapDirection) dir.previous();
		break;
		case FORWARD: run(1, mapa);
		break;
		case BACKWARD: run(-1, mapa);
		break;
		default : System.out.println("ERROR"); 	
		}
		
	}
	public void run(int distance, IWorldMap map){
		
		Position newPosition = new Position(0,0);
        switch(dir) {
            case NORTH: newPosition = pos.add(new Position(distance, 0));
                break;
            case SOUTH: newPosition = pos.add(new Position(-distance, 0));
                break;
            case EAST: newPosition = pos.add(new Position(0, distance));
                break;
            case WEST: newPosition = pos.add(new Position(0, -distance));
                break;
        }
        if(mapa.canMoveTo(newPosition)) {
            pos = newPosition;
            System.out.println(pos.toString());
        }
        else System.out.println("|" + pos.toString());
	}
	
	public Position getPosition(){
		return this.pos;
	}
		
		
}
