package agh.cs.lab2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OptionsParser {

	private Map<String, MoveDirection> optionToDirections;

    public OptionsParser() {
        this.optionToDirections = new HashMap<String, MoveDirection>();
        optionToDirections.put("f", MoveDirection.FORWARD);
        optionToDirections.put("forward", MoveDirection.FORWARD);
        optionToDirections.put("b", MoveDirection.BACKWARD);
        optionToDirections.put("backward", MoveDirection.BACKWARD);
        optionToDirections.put("r", MoveDirection.RIGHT);
        optionToDirections.put("right", MoveDirection.RIGHT);
        optionToDirections.put("l", MoveDirection.LEFT);
        optionToDirections.put("left", MoveDirection.LEFT);
    }

    public List<MoveDirection> parse(String[] options) {
        List<MoveDirection> directions = new ArrayList<MoveDirection>();
        for (String option : options) {
            MoveDirection direction = optionToDirections.get(option);
            if(direction != null) {
                directions.add(direction);
            }
        }
        return directions;
    }
}
