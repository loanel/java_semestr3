package agh.cs.lab2;

public class Position {
	public int x;
	public int y;
	
	public Position(int x, int y){
		this.x=x;
		this.y=y;
	}
	
	public String toString(){
		return "(" + this.x +","+ this.y +")";
	}
	
	public boolean smaller(Position p){
		
		return ((this.x<=p.x) && (this.y<=p.y));
	}
	
	public boolean larger(Position p){
		
		return ((this.x>=p.x) && (this.y>=p.y));
	}
	
	public Position add(Position p){
		return new Position(this.x+p.x, this.y+p.y);
	}
	
	public boolean equals(Object other){
		if (this == other)
		    return true;
		  if (!(other instanceof Position))
		    return false;
		  Position that = (Position) other;
		  return this.x == that.x && this.y == that.y;
	}
	
}
