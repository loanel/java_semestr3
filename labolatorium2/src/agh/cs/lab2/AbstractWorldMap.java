package agh.cs.lab2;

import java.util.ArrayList;
import java.util.List;

public class AbstractWorldMap{
	
	protected List<Car> carList = new ArrayList<>();
	
	public void run(List<MoveDirection> directions) {
		for(int j = 0; j < directions.size(); j=j+1){
			carList.get(j % carList.size()).move(directions.get(j));
		}
	}
}
