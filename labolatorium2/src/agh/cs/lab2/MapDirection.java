package agh.cs.lab2;

public enum MapDirection {
	NORTH("P�noc"),
	EAST("Wsch�d"),
	SOUTH("Po�udnie"),
	WEST("Zach�d");
	private String description;
	
	private MapDirection(String description){
		this.description=description;
	}
	
	public String toString(){
		return this.description;
	}
	
	public Object next(){
	/*  1
	 * 	for(int i=0; i<MapDirection.values().length; i++){
			if(MapDirection.values()[i].toString().equals(this.description)) return MapDirection.values()[(i+1)%MapDirection.values().length].toString();
		}
		return null;
		*/
		/*2
		switch (this.description){
		case "P�noc": return "Wsch�d";
		case "Po�udnie": return "Zach�d";
		case "Wsch�d": return "Po�udnie";
		case "Zach�d": return "P�noc";
		default : throw new Exception("Mozliwe ze dodano nowe pole do enum");
		}
		*/
		/// 3
		return MapDirection.values()[(this.ordinal() + 1)%MapDirection.values().length];
	}
	public Object previous(){
		/*for(int i=0; i<MapDirection.values().length; i++){
			if(MapDirection.values()[i].toString().equals(this.description)){
					if(i==0) return MapDirection.values()[MapDirection.values().length-1].toString();
					return MapDirection.values()[(i-1)%MapDirection.values().length].toString();
				}
		} */
		/// 3
		return MapDirection.values()[(this.ordinal() + MapDirection.values().length-1)%MapDirection.values().length];
	}
}
