package agh.cs.lab2;

import static org.junit.Assert.*;

import org.junit.Test;

public class CarSystemTest {

	@Test
	public void test() {
		Car c1 = new Car();
		Car c2 = new Car();
		c1.move(MoveDirection.FORWARD);
		c2.move(MoveDirection.FORWARD);
		assertEquals(c1.toString(), c2.toString());
	}

}
