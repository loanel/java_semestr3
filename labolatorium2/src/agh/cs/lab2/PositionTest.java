package agh.cs.lab2;

import static org.junit.Assert.*;

import org.junit.Test;

public class PositionTest {

	@Test
	public void testToString() {
		assertEquals("(1,2)", new Position(1,2).toString());
	} 

	@Test
	public void testLarger(){
		assertTrue(new Position(3,4).larger(new Position(2,1)));
		assertFalse(new Position(2,1).larger(new Position(3,4)));
	}
	
	@Test
	public void testSmaller(){
		assertTrue(new Position(2,1).smaller(new Position(3,4)));
	}
	
	@Test
	public void testEquals(){
		assertTrue(new Position(2,1).equals(new Position(2,1)));
	}
	
	@Test
	public void testAdd(){
		assertEquals(new Position(2,2), new Position(1,1).add(new Position(1,1)));
	}
	
}
