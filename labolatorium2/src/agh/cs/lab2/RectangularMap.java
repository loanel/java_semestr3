package agh.cs.lab2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RectangularMap extends AbstractWorldMap implements IWorldMap {

	private Position MaxPos;
	private Position MinPos = new Position(0,0);
	
	public RectangularMap(int width, int height){
		if(width != 0 && height != 0){
			this.MaxPos = new Position(width, height);
		}
		else this.MaxPos = new Position(0,0);
	}
	
	@Override
	public boolean canMoveTo(Position position) {
		if(position.larger(MinPos) && position.smaller(MaxPos)) return !isOccupied(position);
		return false;
	}

	@Override
	public boolean add(Car car) {
		if(!isOccupied(car.getPosition()) 
				&& car.getPosition().smaller(MaxPos) 
				&& car.getPosition().larger(MinPos)){
			carList.add(car);
			return true;
		}
		
		return false;	
	}


	@Override
	public boolean isOccupied(Position position) {
		for(Car c : carList){
			if(c.getPosition().equals(position)) return true;
		}	
		return false;
		
	}

	@Override
	public Object objectAt(Position position) {
		for(Car car : carList){
			if(car.getPosition().equals(position)) return car;
		}
		return null;
	}

}
