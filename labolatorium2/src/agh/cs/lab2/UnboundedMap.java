package agh.cs.lab2;

import java.util.ArrayList;
import java.util.List;

public class UnboundedMap extends AbstractWorldMap implements IWorldMap{
	
	private List<HayStack> hayList = new ArrayList<>();
	
	
	public String toString(){
		Position MAX = new Position(0,0);
		Position MIN = new Position(10000,10000);
		for(Car c : carList){
			if(c.getPosition().x < MIN.x) MIN.x = c.getPosition().x;
			if(c.getPosition().x > MAX.x) MAX.x = c.getPosition().x;
			if(c.getPosition().y < MIN.y) MIN.y = c.getPosition().y;
			if(c.getPosition().y > MAX.y) MAX.y = c.getPosition().y;
		}
		for(HayStack hay : hayList){
			if(hay.getPosition().x < MIN.x) MIN.x = hay.getPosition().x;
			if(hay.getPosition().x > MAX.x) MAX.x = hay.getPosition().x;
			if(hay.getPosition().y < MIN.y) MIN.y = hay.getPosition().y;
			if(hay.getPosition().y > MAX.y) MAX.y = hay.getPosition().y;
		}
		MapVisualizer projektor = new MapVisualizer();
		return projektor.dump(this, MIN, MAX);
	}
	
	public UnboundedMap(List<HayStack> lista){
		this.hayList = lista;
	}
	@Override
	public boolean canMoveTo(Position position) {
		return !isOccupied(position);
	}

	@Override
	public boolean add(Car car) {
		if(!isOccupied(car.getPosition())){
			carList.add(car);
			return true;
		}
		return false;
	}


	@Override
	public boolean isOccupied(Position position) {
		for(Car c : carList){
			if(c.getPosition().equals(position)) return true;
		}
		for(HayStack h : hayList){
			if(h.getPosition().equals(position)) return true;
		}
		return false;
	}

	@Override
	public Object objectAt(Position position) {
		for(Car car : carList){
			if(car.getPosition().equals(position)) return car;
		}
		for(HayStack hay : hayList){
			if(hay.getPosition().equals(position)) return hay;
		}
		return null;
	}

}
