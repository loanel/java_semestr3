package agh.cs.lab2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarSystem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Position position1 = new Position(1,2);
		System.out.println(position1);
		Position position2 = new Position(-2,1);
		System.out.println(position2);
		System.out.println(position1.add(position2));
		
		MapDirection x = MapDirection.NORTH;
		System.out.println(x.toString());
		System.out.println(x.next());
		System.out.println(x.previous());
	/*	
		OptionsParser parser = new OptionsParser();
	    String[] options = new String[4];
	    options[0] = "right";
	    options[1] = "f";
	    options[2] = "forward";
	    options[3] = "forward";
	    List<MoveDirection> directions = parser.parse(options);
	    Car car = new Car();
	    for(MoveDirection direction : directions) {
	       car.move(direction);
	       System.out.println(car);
	    }
		
	*/
	
	    List<MoveDirection> directions = new OptionsParser().parse(args);
	    List<HayStack> hays = new ArrayList<>();
	    
	    hays.add(new HayStack(new Position(-4,-4)));
	    hays.add(new HayStack(new Position(7,7)));
	    hays.add(new HayStack(new Position(3,6)));
	    hays.add(new HayStack(new Position(2,0)));
	    
	    IWorldMap map = new UnboundedMap(hays);
	    Car auto1 = new Car(map);
	    Car auto2 = new Car(map,3,4);
	    
	    map.add(auto1);
	    map.add(auto2);
	    
	    map.run(directions);
	    
	    
	    System.out.println(auto1.toString());
	    System.out.println(auto2.toString());

	    	   
	}

}
