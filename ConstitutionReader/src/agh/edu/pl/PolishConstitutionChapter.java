package agh.edu.pl;

import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;

public class PolishConstitutionChapter implements Chapter {
	
	private List<Article> articles = new ArrayList<>();
	private int number;
	
	public PolishConstitutionChapter(int number){
		this.number = number;
	}
	public int getNumber() {
		return this.number;
	}

	@Override
	public void addArticle(Article article) {
		articles.add(article);
	}
	
	public String toString(){
		StringBuilder chapter = new StringBuilder();
		for(Article a : articles){
			chapter.append("Art." + a.getNumber() + "\n" + a.toString());
		}
		return chapter.toString();
	}

}
