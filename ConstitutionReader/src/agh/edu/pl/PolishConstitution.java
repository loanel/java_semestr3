package agh.edu.pl;

import java.util.ArrayList;
import java.lang.ArrayIndexOutOfBoundsException;
import java.util.List;
import java.lang.StringBuilder;

public class PolishConstitution implements Constitution {
	
	private List<Chapter> chapters = new ArrayList<>();
	private List<Article> articles = new ArrayList<>();
	@Override
	public void addArticle(Article article) {
		articles.add(article);
	}

	@Override
	public void addChapter(Chapter chapter) {
		chapters.add(chapter);
	}

	@Override
	public String printArticle(int number) throws ArrayIndexOutOfBoundsException { 
		if (number-1 < articles.size() && number - 1 >= 0) return "Art." + articles.get(number-1).getNumber() + "\n" +articles.get(number-1).toString();
		else throw new ArrayIndexOutOfBoundsException("An article with this number doesn't exist in this constitution");
	}

	@Override
	public String printArticles(int first, int last) throws ArrayIndexOutOfBoundsException {
		StringBuilder builder = new StringBuilder();
		if((first > 0 && first < articles.size()) && (last>0 && last<articles.size())){
			for(int i = first; i<= last; i++){
					builder.append(printArticle(i));
				}
				return builder.toString();
		}
		else throw new ArrayIndexOutOfBoundsException("This constitution doesn't contain this range of indexes");
	}

	@Override
	public String printChapter(int number) throws ArrayIndexOutOfBoundsException {
		if (number-1 < chapters.size() && number - 1 >= 0) return "Rozdzial." + chapters.get(number-1).getNumber() + "\n" +chapters.get(number-1).toString();
		else throw new ArrayIndexOutOfBoundsException("A chapter with this number doesn't exist in this constitution");
	}

}
