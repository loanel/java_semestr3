package agh.edu.pl;

public interface Chapter {
	// wypisuje wszystkie artykuly wraz z ich trescia
	String toString();
	// wypisuje numer danego rozdzialu
	int getNumber();
	// dodanie artyku�u do listy artyku��w rozdzia�u
	void addArticle(Article article);
}
