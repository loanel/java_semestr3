package agh.edu.pl;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.StringBuilder;

public class PolishConstitutionParser implements CParser {

	@Override
	public Constitution buildConstitution(String file_path) throws IOException{
		//inicjalizacja zmiennych
		Constitution polishconst = new PolishConstitution();
		int articleCounter = 0;
		int chapterCounter = 0;
		Chapter currentChapter = null;
		Article currentArticle = null;
		boolean startAddingText = false;
		boolean pauseFixReq = false;
		//inicjalizacja pliku i readera
		BufferedReader br = new BufferedReader(new FileReader(file_path));
		
		// budowanie strukextury klas
		String inputText;
		String splitter[];
		while((inputText = br.readLine()) != null){
			if(inputText.length()>1 && !inputText.matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]") && !inputText.matches("©Kancelaria Sejmu") && !(Character.isUpperCase(inputText.charAt(0)) && Character.isUpperCase(inputText.charAt(1)))){ // odrzucamy 
				StringBuilder newText = new StringBuilder(inputText);
				splitter = inputText.split(" ", 2);
				switch (splitter[0]){
					case "Rozdzia≥":  currentChapter = new PolishConstitutionChapter(chapterCounter + 1);  // "Rozd" - tworzymy nowy rozdzial
									  polishconst.addChapter(currentChapter);
									  startAddingText = true; // dodawanie nowego tekstu rozpoczynamy od pierwszego napotkanego rozdzialu w danej konstytucji
									  chapterCounter++;
									  break;
					case "Art.":  currentArticle = new PolishConstitutionArticle(articleCounter + 1); // "Art." - tworzymy nowy arykul
								  polishconst.addArticle(currentArticle);
								  currentChapter.addArticle(currentArticle);
								  articleCounter++;
								  break;
					default : if(pauseFixReq){
								  newText = fixText(newText);
								  pauseFixReq = false;
							  }
							  if(newText.charAt(newText.length()-1) == '-') { // odkrylismy "-" na koncu linii 
								  newText.setLength(newText.length()-1); // usuwam "-" z tekstu
								  pauseFixReq = true; // ustawiam flage wymagania naprawy tekstu
							  }
							  if(!pauseFixReq) newText.append("\n"); // zawsze dodajemy znak \n, o ile nie musimy naprawic struktury tekstu
							  if(startAddingText) currentArticle.addParagraph(newText.toString());
				}
			}	
		}
		br.close();
		return polishconst;
	}
	public StringBuilder fixText(StringBuilder newText){
		boolean pauseOccured = false;
		  int i=0;
		  while(!pauseOccured && i<newText.length()){
			  if(newText.charAt(i) == ' '){
				  pauseOccured = true;
				  newText.setCharAt(i, '\n');  // znajdujemy pierwsza spacje w nowej lini i zamieniamy ja na \n
			  }
			  i++;
		  }
		return newText;
		
	}

}
