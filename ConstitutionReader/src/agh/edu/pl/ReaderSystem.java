package agh.edu.pl;
import java.io.IOException;
import java.lang.ArrayIndexOutOfBoundsException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.StringBuilder;
import java.lang.IllegalArgumentException;

public class ReaderSystem {

	public static void main(String[] args) {
		OptionsParser oParser = new PolishConstitutionOptionsParser();
		System.out.println(oParser.parseOptions(args));
	}	
}
