package agh.edu.pl;

public interface OptionsParser {
	
	// parser argumentow wpisanych do programu, wykonujacy odpowiednie opcje na konstytucji
	String parseOptions(String[] options);
	
}
